package com.company;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.List;
import java.util.StringTokenizer;

/**
 * @author Mhosein Abbasi, 3/2/2022 10:45 AM
 */
public class SystemInformation {
    public static void main(String[] args) {
        getSystemInformation();
    }

    private static void getSystemInformation() {
        try {
            String OSName = System.getProperty("os.name");
            if (OSName.contains("Windows")) {
                getWindowsMotherBoardInformation();
                getWindowsCpuInformation();
                getWindowsMemoryInformation();
                getWindowsHardDiscInformation();
                getWindowsMacAddress();
            } else {
                String linuxMotherBoard_serialNumber = GetLinuxMotherBoard_serialNumber();
                System.out.println("linux Motherboard_serialNumber is : " + linuxMotherBoard_serialNumber);

            }
        } catch (Exception E) {
            System.err.println("System MotherBoard Exp : " + E.getMessage());
        }
    }

    private static void getWindowsMotherBoardInformation() {
        Command command = new Command();
        List<String> motherboardModel = command.windowsExecuteCommand("wmic baseboard get Manufacturer, serialnumber");
        for (int i = 0; i < motherboardModel.size(); i++) {
            if (i == 0) {
                continue;
            }
            StringTokenizer stringTokenizer = new StringTokenizer(motherboardModel.get(i), " ");
            System.out.println("windows Motherboard_manufacturer is : " + stringTokenizer.nextToken());
            System.out.println("windows Motherboard_serialNumber is : " + stringTokenizer.nextToken());
        }
    }

    private static void getLinuxMotherBoardInformation() {
        Command command = new Command();
        List<String> motherboardModel = command.linuxExecuteCommand("ls -la");
    }

    private static void getWindowsCpuInformation() {
        Command command = new Command();
        List<String> cpuModel = command.windowsExecuteCommand("wmic CPU get NAME");
        for (int i = 0; i < cpuModel.size(); i++) {
            if (i == 0) {
                continue;
            }
            System.out.println("windows CPU_model is : " + cpuModel.get(i));
            System.out.println("windows CPU_serialNumber is : " + getWindowsCPU_SerialNumber());
        }
    }

    private static void getWindowsMemoryInformation() {
        Command command = new Command();
        List<String> memoryChip = command.windowsExecuteCommand("wmic memorychip get serialnumber");
        for (int i = 0; i < memoryChip.size(); i++) {
            if (i == 0) {
                continue;
            }
            System.out.println((i) + "-> windows Memory_serialNumber is : " + memoryChip.get(i));
        }
    }

    private static void getWindowsHardDiscInformation() {
        Command command = new Command();
        List<String> hardDisk = command.windowsExecuteCommand("wmic diskdrive get model, serialnumber");
        for (int i = 0; i < hardDisk.size(); i++) {
            if (i == 0) {
                continue;
            }
            StringTokenizer stringTokenizer = new StringTokenizer(hardDisk.get(i), " ");
            String model = stringTokenizer.nextToken();
            String serialnumber = stringTokenizer.nextToken();
            System.out.println((i) + "-> windows Disk_drive_model is : " + model);
            System.out.println((i) + "-> windows Disk_drive_serialNumber is : " + serialnumber);
        }
    }

    private static void getWindowsMacAddress() {
        try {
            InetAddress ip = null;
            try {
                ip = InetAddress.getLocalHost();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
            if (ip != null) {
                System.out.println("windows current IP address : " + ip.getHostAddress());
            }
            Enumeration<NetworkInterface> networks = NetworkInterface.getNetworkInterfaces();
            while (networks.hasMoreElements()) {
                NetworkInterface network = networks.nextElement();
                byte[] mac = network.getHardwareAddress();

                if (mac != null) {
                    System.out.print("windows current MAC address : ");

                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < mac.length; i++) {
                        sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
                    }
                    System.out.println(sb);
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    private static String GetLinuxMotherBoard_serialNumber() {
        String command = "dmidecode -s baseboard-serial-number";
        String sNum = null;
        try {
            Process SerNumProcess = Runtime.getRuntime().exec(command);
            BufferedReader sNumReader = new BufferedReader(new InputStreamReader(SerNumProcess.getInputStream()));
            sNum = sNumReader.readLine().trim();
            SerNumProcess.waitFor();
            sNumReader.close();
        } catch (Exception ex) {
            System.err.println("Linux Motherboard Exp : " + ex.getMessage());
            sNum = null;
        }
        return sNum;
    }

    private static String GetLinuxMemory_serialNumber() {
        String command = "dmidecode -s baseboard-serial-number";
        String sNum = null;
        try {
            Process SerNumProcess = Runtime.getRuntime().exec(command);
            BufferedReader sNumReader = new BufferedReader(new InputStreamReader(SerNumProcess.getInputStream()));
            sNum = sNumReader.readLine().trim();
            SerNumProcess.waitFor();
            sNumReader.close();
        } catch (Exception ex) {
            System.err.println("Linux Motherboard Exp : " + ex.getMessage());
            sNum = null;
        }
        return sNum;
    }

    private static String getWindowsCPU_SerialNumber() {
        StringBuilder result = new StringBuilder();
        try {
            File file = File.createTempFile("realhowto", ".vbs");
            file.deleteOnExit();
            FileWriter fw = new FileWriter(file);
            String vbs1
                    = "Set objWMIService = GetObject(\"winmgmts:\\\\.\\root\\cimv2\")\n"
                    + "Set colItems = objWMIService.ExecQuery _ \n"
                    + " (\"Select * from Win32_Processor\") \n"
                    + "For Each objItem in colItems \n"
                    + " Wscript.Echo objItem.ProcessorId \n"
                    + " exit for ' do the first cpu only! \n"
                    + "Next \n";
            fw.write(vbs1);
            fw.close();
            Process p = Runtime.getRuntime().exec("cscript //NoLogo " + file.getPath());
            BufferedReader input = new BufferedReader(
                    new InputStreamReader(p.getInputStream()));
            String line;
            while ((line = input.readLine()) != null) {
                result.append(line);
            }
            input.close();
        } catch (Exception E) {
            System.err.println("Windows CPU Exp : " + E.getMessage());
        }
        return result.toString().trim();
    }
}
